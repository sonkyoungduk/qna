# Objectives
마이크로서비스 아키텍처에서 핵심 요소 중 하나인 이벤트 프로그래밍을 소개합니다.

## Environment
- Spring Boot
- JPA(Java Persistence API)
- Spring Cloud Stream(Kafka)
- CQRS(Command Query Responsibility Segregation)
- MariaDB or H2

## Account Microservice
사용자를 관리하는 심플한 마이크로서비스를 구현합니다.  
이 서비스 개발을 통해 백엔드 마이크로서비스의 구조를 이해합니다.  

## Spring Event
스프링이 제공하는 ApplicationEventPublisher, @EventListener를 이용하여 하나의 마이크로서비스에서 이벤트 처리 방법을 소개합니다.  
실습 서비스는 회원 가입/탈퇴시 현재 가입한 회원수를 실시간으로 별도의 테이블에 업데이트합니다.  

## Kafka & Spring Cloud Stream
Kafka의 핵심 개념을 설명합니다.  
Kafka Command Line Interface로 메시지를 발행과 소비를 실습합니다.  
Spring Cloud Stream을 활용하여 마이크로서비스간에 이벤트를 발행/소비하는 방법을 소개합니다.  

## etc
이전 교육에서 사용한 MariaDB의 계정 및 비밀번호입니다.
- root/poscoict
- account/account
